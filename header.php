<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package storefront
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2.0">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
  	<script>
    	AOS.init();
  	</script>
	<?php do_action( 'storefront_before_site' ); ?>

	<div id="page" class="hfeed site">
		<?php do_action( 'storefront_before_header' ); ?>

		<header id="masthead" class="site-header" role="banner" style="<?php storefront_header_styles(); ?>">

			<?php
		/**
		 * Functions hooked into storefront_header action
		 *
		 * @hooked storefront_header_container                 - 0
		 * @hooked storefront_skip_links                       - 5
		 * @hooked storefront_social_icons                     - 10
		 * @hooked storefront_site_branding                    - 20
		 * @hooked storefront_secondary_navigation             - 30
		 * @hooked storefront_product_search                   - 40
		 * @hooked storefront_header_container_close           - 41
		 * @hooked storefront_primary_navigation_wrapper       - 42
		 * @hooked storefront_primary_navigation               - 50
		 * @hooked storefront_header_cart                      - 60
		 * @hooked storefront_primary_navigation_wrapper_close - 68
		 */
		do_action( 'storefront_header' );
		?>

		</header><!-- #masthead -->
		<div class="cabecera">
				<div class="cont2">
					<nav class="navbar-expand-lg header__nav menu_principal">
					  	<ul id="main__menu" class="nav navbar-nav">
            				<li id="menu-item-18" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-8 current_page_item menu-item-18">
              					<a href="https://www.tech-hospital.com/" class="nav-link t-white b-hover-red t-hover-white">Inicio</a>
           					</li>
            				<li id="menu-item-17" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17">
              					<a href="https://www.tech-hospital.com/sobre-nosotros/" class="nav-link t-white b-hover-red t-hover-white">Nosotros</a>
            				</li>
          				</ul>
					</nav>
				</div>
				<div class="logo">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportados-10.png" alt="">
				</div>
				<div class="cont2">
					<nav class="navbar-expand-lg header__nav menu_principal">
					  	<ul id="main__menu" class="nav navbar-nav">
            				<li id="menu-item-18" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-8 current_page_item menu-item-18">
              					<a href="https://www.tech-hospital.com/" class="nav-link t-white b-hover-red t-hover-white">Contacto</a>
           					</li>
            				<li id="menu-item-17" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17">
              					<a href="https://www.tech-hospital.com/sobre-nosotros/" class="nav-link t-white b-hover-red t-hover-white">Mision y Vision</a>
            				</li>
          				</ul>
					</nav>
				</div>
		</div>
		<?php
	/**
	 * Functions hooked in to storefront_before_content
	 *
	 * @hooked storefront_header_widget_region - 10
	 * @hooked woocommerce_breadcrumb - 10
	 */
	do_action( 'storefront_before_content' );
	?>

		<div id="content" class="site-content" tabindex="-1">
			<div >

				<?php
		do_action( 'storefront_content_top' );