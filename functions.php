<?php
/**
 * Storefront engine room
 *
 * @package storefront
 */

/**
 * Assign the Storefront version to a var
 */
$theme              = wp_get_theme( 'storefront' );
$storefront_version = $theme['Version'];

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 980; /* pixels */
}

$storefront = (object) array(
	'version'    => $storefront_version,

	/**
	 * Initialize all the things.
	 */
	'main'       => require 'inc/class-storefront.php',
	'customizer' => require 'inc/customizer/class-storefront-customizer.php',
);
function ayuda(){
	echo "
	<a class=\"ayuda\" href=\"\">AYUDA</a>";
}

add_action( 'storefront_header', 'ayuda', 60 );
// add_action( 'storefront_header', 'storefront_header_cart',    60 );
add_action( 'storefront_header', 'storefront_product_search', 60 );

add_action( 'storefront_header', 'the_custom_logo', 50 );


require 'inc/storefront-functions.php';
require 'inc/storefront-template-hooks.php';
require 'inc/storefront-template-functions.php';

if ( class_exists( 'Jetpack' ) ) {
	$storefront->jetpack = require 'inc/jetpack/class-storefront-jetpack.php';
}

if ( storefront_is_woocommerce_activated() ) {
	$storefront->woocommerce            = require 'inc/woocommerce/class-storefront-woocommerce.php';
	$storefront->woocommerce_customizer = require 'inc/woocommerce/class-storefront-woocommerce-customizer.php';

	require 'inc/woocommerce/class-storefront-woocommerce-adjacent-products.php';

	require 'inc/woocommerce/storefront-woocommerce-template-hooks.php';
	require 'inc/woocommerce/storefront-woocommerce-template-functions.php';
	require 'inc/woocommerce/storefront-woocommerce-functions.php';
}

if ( is_admin() ) {
	$storefront->admin = require 'inc/admin/class-storefront-admin.php';

	require 'inc/admin/class-storefront-plugin-install.php';
}

/**
 * NUX
 * Only load if wp version is 4.7.3 or above because of this issue;
 * https://core.trac.wordpress.org/ticket/39610?cversion=1&cnum_hist=2
 */
if ( version_compare( get_bloginfo( 'version' ), '4.7.3', '>=' ) && ( is_admin() || is_customize_preview() ) ) {
	require 'inc/nux/class-storefront-nux-admin.php';
	require 'inc/nux/class-storefront-nux-guided-tour.php';

	if ( defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '3.0.0', '>=' ) ) {
		require 'inc/nux/class-storefront-nux-starter-content.php';
	}
}

/**
 * Note: Do not add any custom code here. Please use a custom plugin so that your customizations aren't lost during updates.
 * https://github.com/woocommerce/theme-customisations
 */
 // personalizacion
// Scripts & Styles
function smart_styles () {
	// wp_register_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array(), '4.1.1' );
	wp_register_style('icons', 'https://use.fontawesome.com/releases/v5.2.0/css/all.css', array(), '5.2.0');
	wp_register_style('icons2', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css', array(), '4.3.0');
	wp_register_style('style', get_template_directory_uri() . '/assets/css/tech.css', array(), '72.06.3');
	wp_register_style('fonts', 'https://fonts.googleapis.com/css?family=Montserrat:400', array('style'), '1.0.0');
	wp_register_style('ws', 'https://www.w3schools.com/w3css/4/w3.css', array(), '1.0.0');
	// wp_enqueue_style('bootstrap');
	wp_enqueue_style('icons');
	wp_enqueue_style('icons2');
	wp_enqueue_style('style');
	wp_enqueue_style('fonts');
	wp_enqueue_style('ws');
	wp_enqueue_script('jquery');
	// wp_enqueue_script( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js', array('jquery'), '4.0.0', true );
	// wp_enqueue_script('script', get_template_directory_uri() . '/assets/js/script.js', array('jquery'), '1.0.0', true);
  }
  add_action('wp_enqueue_scripts', 'smart_styles');

  
function storefront_credit() {
	?>
	<section id="contas">
	<div class="title__section5">
		
  		<div class="centrado5"><a href="#">Quienes Somos</a></div>
	</div>

	<div class="title__section6">
		
  		<div class="centrado6"><a href="#">Mision y Vision</a></div>
	</div>

	<div class="title__section7">
		
  		<div class="centrado7"><a href="#">Valores</a></div>
	</div>

	<!--
	<div class="title__section3">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/punto.png" alt="Nuevos Ingresos">
  		<div class="centrado3"><a href="#">Conoce Mas</a></div>
	</div>
	<div class="title__section4">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/punto.png" alt="Nuevos Ingresos">
  		<div class="centrado3"><a href="#">Politicas de <br>Privacidad</a></div>
	</div>
	<div class="title__section5">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/punto.png" alt="Nuevos Ingresos">
  		<div class="centrado3"><a href="#">Politicas de <br>Empresa</a></div>
	</div>


	<div class="title__section6">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/punto.png" alt="Nuevos Ingresos">
  		<div class="centrado3"><a href="#">Servicio al Cliente</a></div>
	</div>
	<div class="title__section7">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/punto.png" alt="Nuevos Ingresos">
  		<div class="centrado3"><a href="#">Contactanos</a></div>
	</div>
	<div class="title__section8">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/punto.png" alt="Nuevos Ingresos">
  		<div class="centrado3"><a href="#">Entrega</a></div>
	</div>
	<div class="title__section9">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/punto.png" alt="Nuevos Ingresos">
  		<div class="centrado3"><a href="#">Garantias</a></div>
	</div>




	<div class="title__section10">
	<div class="cont1">
				<div class="buscar">
					<form role="search" method="get" action="<?php echo esc_url(home_url('/'));?>">
						<input type="search" id="woocommerce-product-search-field-1" class="search-field round"
							value="" name="s">
						<button type="submit" value="Buscar" class="hidden">Buscar</button>
						<input type="hidden" name="post_type" value="product">
					</form>
					<div class="imagen">
					</div>
				</div>
	</div>
	<div class="centrado10"><a href="#">Sing Up</a></div>
	</div>























	<div class="title__section10">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportados-11.png" alt="Nuevos Ingresos">
		<div class="centrado11">
			<form class="form-inline my-2 my-lg-0" role="search" method="get" action="<?php echo esc_url(home_url('/'));?>">
  				<input type="search" id="woocommerce-product-search-field-1" class="search-field"value="" name="s">
			</form>
		</div>
  		<div class="centrado10"><a href="#">Sing Up</a></div>
	</div>

	<div class="contenedor2">
 		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportados-11.png" />
  		<div class="centrado2">
  			<form class="form-inline my-2 my-lg-0" role="search" method="get" action="<?php echo esc_url(home_url('/'));?>">
  				<input type="search" id="woocommerce-product-search-field-1" class="search-field"value="" name="s">
			</form>
  		</div>
	</div>-->
	


	
  
	</section>
	
    <div class="site-info">
	<div class="b-black t-white t-center d-flex a-center j-center" style="padding:0.5rem;">
      <span class="rights__text">Este sitio fue creado por <a href="https://techwebgt.com" class="text-white">TechWeb</a> - © <?php echo date('Y'); ?>
      </span>
    </div>
    </div><!-- .site-info -->
    <?php
}


//menu
function mis_menus() {
	register_nav_menus(
	  array(
		'principal' => __( 'Menu principal' ),
	  )
	);
  }
  add_action( 'init', 'mis_menus' );
  
  //crear clase para <a>
  add_filter( 'nav_menu_link_attributes', 'clase_menu', 10, 3 );
  
  function clase_menu($atts, $item, $args){
	$class = 'nav-link';
	$atts['class'] = $class;
	return $atts;
  }  