<?php
/**
 * The template used for displaying page content in template-homepage.php
 *
 * @package storefront
 */

?>
<?php
$featured_image = get_the_post_thumbnail_url( get_the_ID(), 'thumbnail' );
?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="<?php storefront_homepage_content_styles(); ?>" data-featured-image="<?php echo esc_url( $featured_image ); ?>">
	<div class="col-full">
		<?php
		/**
		 * Functions hooked in to storefront_page add_action
		 *
		 * @hooked storefront_homepage_header      - 10
		 * @hooked storefront_page_content         - 20
		 */
		// do_action( 'storefront_homepage' );
		?>
	</div>
</div><!-- #post-## -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>



<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="<?php echo get_template_directory_uri(); ?>/assets/images/banner1.png" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="<?php echo get_template_directory_uri(); ?>/assets/images/banner1.png" alt="Second slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>


<section id="sobre">

<div class="sobres" data-aos="fade-right">
  <h1>¿Quienes Somos?</h1>
  <p>Reus Pharma nace en el año 2016 en Guatemala como una empresa farmaceutica
que desarrolla y comercializa productos propios y distribuye productos de empresas
que confían en nuestros servicios.
Contamos con equipo de visita medica para los laboratorios que no tienen personal en 
Guatemala y tambien un equipo de ventas que cubre todo el territorio nacional. Cubrimos los
principales clientes del pais y estamos en constante busqueda de productos que puedan
mejorar la calidad de vida de los guatemaltecos.</p>
</div>

</section>

<section id="misionvision">

<div class="title__section12" data-aos="fade-right">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportados-15.png" alt="Nueos Ingresos">
</div>

<div class="title__section4" data-aos="fade-left">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportados-16.png" alt="Nueos Ingresos">
  <div class="centrado4">
  <p>Contribuir a la salud de los guatemaltecos con productos de buena calidad a un precio accessible 
a través de un equipo altamente calificado y un excelente servicio.</p>
  </div>
</div>


<div class="title__section13" data-aos="fade-left">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportados-08.png" alt="Nueos Ingresos">
</div>

<div class="title__section3" data-aos="fade-right">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportados-16.png" alt="Nueos Ingresos">
  <div class="centrado3">
  <p>
Ser el distribuidor y comercializador mas importante para nuestros clientes
y para nuestros socios comerciales. Basados en nuestros valores corporativos
y en mejorar la calidad de vida de las personas.</p>
  </div>
</div>

</section>

<section id="valores">

<div class="title__section10">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/valors.png" alt="Nueos Ingresos">
</div>

<div class="title__section11">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/fondovalrs.png" alt="Nueos Ingresos">
</div>
<!--
<div class="title__section14">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportados-18.png" alt="Nueos Ingresos">
</div>

<div class="title__section15">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportados-17.png" alt="Nueos Ingresos">
</div>

<div class="title__section16">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportados-22.png" alt="Nueos Ingresos">
</div>

<div class="title__section17">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportados-21.png" alt="Nueos Ingresos">
</div>-->



</section>

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
	<div class="row">
			<div class="col-3"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportados-05.png" alt="First slide"></div>
			<div class="col-3"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportados-11.png" alt="First slide"></div>
			<div class="col-3"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportados-05.png" alt="First slide"></div>
			<div class="col-3"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportados-11.png" alt="First slide"></div>
		</div>
    </div>
    <div class="carousel-item">
	<div class="row">
			<div class="col-3"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportados-05.png" alt="First slide"></div>
			<div class="col-3"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportados-11.png" alt="First slide"></div>
			<div class="col-3"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportados-05.png" alt="First slide"></div>
			<div class="col-3"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportados-11.png" alt="First slide"></div>
		</div>
    </div>
    <div class="carousel-item">
	<div class="row">
			<div class="col-3"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportados-05.png" alt="First slide"></div>
			<div class="col-3"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportados-11.png" alt="First slide"></div>
			<div class="col-3"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportados-05.png" alt="First slide"></div>
			<div class="col-3"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/exportados-11.png" alt="First slide"></div>
		</div>
    </div>
  </div>
</div>
	
</div>
	